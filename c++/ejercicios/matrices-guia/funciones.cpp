#include <iostream>
#include <string>
using namespace std;

#define MAX_FILAS 30
#define MAX_COLUMNAS 25

string printMatriz (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols) {
    string r = "";

    for (int i = 0; i < rows; i++) {
        r.append("[ ");
        for (int j = 0; j < cols; j++) {
            r.append(" ");
            r.append(std::to_string(matriz[i][j]));
            r.append(" ");
        }
        r.append(" ]\n");
    }
    return r;
}

float promedio (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols) {
    int contador = 0, sumador = 0;
    
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            sumador += matriz[i][j];
            contador += 1;
        }
    }

    return float(sumador) / float(contador);
}

void mayor (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int& max, int& xpos, int& ypos) {
    int imax = 0;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (matriz[i][j] > imax) {
                imax = matriz[i][j];
                xpos = i + 1;
                ypos = j + 1;
            }
        }
    }

    max = imax;
}

void sumcolum (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    for (int i = 0; i < cols; i++) {
        r[i] = 0;
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            r[j] += matriz[i][j];
        }
    }
}

void maxfila (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    for (int i = 0; i < rows; i++) {
        r[i] = 0;
    }

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (matriz[i][j] > r[i]) {
                r[i] = matriz[i][j];
            }
        }
    }
}