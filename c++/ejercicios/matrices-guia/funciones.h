#include <iostream>
#include <string>
using namespace std;

#define MAX_FILAS 30
#define MAX_COLUMNAS 25

string printMatriz (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols);
float promedio (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols);
void mayor (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int& max, int& xpos, int& ypos);
void sumcolum (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]);
void maxfila (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]);