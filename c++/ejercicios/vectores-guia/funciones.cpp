void maximosArray (int in[], int tamanio, int r[]) {
    // Performance--
    int maximo = 0;
    int indice = 0;
    int contador = 0;

    for (int i = 0; i < tamanio; i++) {
        if (in[i] >= maximo) {
            maximo = in[i];
            r[indice] = i;
            indice++;
            contador++;
        }
    }

    for (int i = 0; i < tamanio; i++) {
        if (i >= contador) {
            r[i] = 0;
        }
    }
}

void extermos (int a[], int b[], int c[], int tamanio) {
    int contadorBorde = 1;

    for (int i = 0; i < tamanio; i++) {
        c[i] = a[i] + b[tamanio - contadorBorde];
        contadorBorde++;
    }
}

void laPesquisa (float r[], int tamanio, float pesquisa, int& posicionCoincidente, int& posicionLeft, int& posicionRight, bool& menorMayor) {
    // Estoy suponiendo que siempre se ingresa un array con valores distintos en orden creciente
    if (pesquisa < r[0] || pesquisa > r[tamanio - 1]) {
        posicionCoincidente = -1;
        posicionLeft = -1;
        posicionRight = -1;
        menorMayor = true;
    } else {
        for (int i = 0; i < tamanio; i++) {
            if (r[i] == pesquisa) {
                posicionCoincidente = i;
                menorMayor = false;

                if (posicionCoincidente == 0) {
                    posicionLeft = -1;
                } else {
                    posicionLeft = i - 1;
                }

                if (posicionCoincidente == tamanio - 1) {
                    posicionRight = -1;
                } else {
                    posicionRight = i + 1;
                }
            } else {
                if (pesquisa > r[i] && pesquisa < r[i + 1]) {
                    posicionCoincidente = -1;
                    posicionLeft = i;
                    posicionRight = i + 1; 
                    menorMayor = false;
                } else {
                    continue;
                }
            }
        }
    }
}

bool palindromo (char r[], int tamanio) {
    bool esPalindromo = true;

    for (int i = 0; i < tamanio; i++) {
        if (r[i] != r[tamanio - (i + 1)]) {
            esPalindromo = false;
        }
    }

    return esPalindromo;
}