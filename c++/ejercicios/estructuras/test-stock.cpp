#include <iostream>
#include "../../test/catch.hpp"
#include "estructuras.h"
#include "funciones.h"
using namespace std;

// Fixtures ingredientes
Ingrediente pan = {"pan", 20, 40};
Ingrediente carne = {"carne", 16, 20};
Ingrediente queso = {"queso", 10, 20};
Ingrediente jamon = {"jamon", 5, 20};
Receta hamburguesa = {
    "hamburguesa",
    {{carne, 1}, {pan, 2}, {queso, 1}},
    72.5
};

TEST_CASE( "Hay stock para hacer una cangreburger?" ) {
    bool checkStock = stock(hamburguesa);

    REQUIRE( checkStock );
}

TEST_CASE( "Cocinar" ) {
    int stockPanPrevio = pan.stock;
    int stockCarnePrevio = carne.stock;
    int stockQuesoPrevio = queso.stock;

    cocinar(hamburguesa);

    REQUIRE( pan.stock == stockPanPrevio - 2 );
    REQUIRE( carne.stock == stockCarnePrevio - 1 );
    REQUIRE( queso.stock == stockQuesoPrevio - 1 );
}

TEST_CASE( "Stock critico de pan" ) {
    float valorCritico = pan.stockMaximo * 0.3;

    REQUIRE( pan.stock > valorCritico );

    for (int i = 0; i < 4; i++) {
        cocinar(hamburguesa);
    }

    bool estado = stockCritico(pan);

    REQUIRE( estado == pan.stock < valorCritico );
}