#include <iostream>
using namespace std;

struct Punto {
    int X;
    int Y;
};

struct Ingrediente {
    string nombre;
    int stock;
    int stockMaximo;
};

struct Receta {
    struct IngredienteInterno {
        Ingrediente& ing;
        int cantidad;
    };

    string nombre;
    IngredienteInterno ingredientes[3];
    float precio;
};