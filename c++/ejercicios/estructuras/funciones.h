int distancia (Punto posFinal, Punto posInicial);
void ecuacionRectaDosPuntos (Punto posFinal, Punto posInicial, float& pendiente, float& ordenada);
void cincoPuntos (Punto posInicial, Punto posFinal, float puntos[5]);
bool stock (Receta& receta);
void cocinar (Receta& receta);
bool stockCritico (Ingrediente& ing);