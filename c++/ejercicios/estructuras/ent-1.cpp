#include <iostream>
using namespace std;

// Estructuras
struct Ingrediente {
    string nombre;
    int stock;
    int stockMaximo;
};

struct Receta {
    struct IngredienteInterno {
        Ingrediente& ing;
        int cantidad;
    };

    string nombre;
    IngredienteInterno ingredientes[3];
    float precio;
};

// Funciones
bool chequearStock (Receta& receta) {
    bool r = true;
    for (int i = 0; i < 3; i++) {
        if (receta.ingredientes[i].ing.stock < receta.ingredientes[i].cantidad) {
            r = false;
            printf("No hay suficiente stock de %s para completar la orden.\n", receta.ingredientes[i].ing.nombre.c_str());
        }
    }
    return r;
}

void cocinar (Receta& receta) {
    printf("Orden de %s enviada.\n", receta.nombre.c_str());
    for (int i = 0; i < 3; i++) {
        receta.ingredientes[i].ing.stock -= receta.ingredientes[i].cantidad; 
    }
}

bool stockCritico (Ingrediente& ing) {
    bool r = false;
    float valorCritico = ing.stockMaximo * 0.3;

    if (float(ing.stock) < valorCritico) {
        r = true;
    }

    return r;
}

void recargar (Ingrediente& ing, int cantidad) {
    ing.stock += cantidad;
}

int main () {
    // Setup ingredientes
    Ingrediente pan = {"pan", 20, 40};
    Ingrediente carne = {"carne", 16, 20};
    Ingrediente queso = {"queso", 10, 20};
    Ingrediente jamon = {"jamon", 5, 20};

    Ingrediente * listaIngredientes[4] = {&pan, &carne, &queso, &jamon};

    // Setup recetas
    Receta hamburguesa = {
        "hamburguesa",
        {{carne, 1}, {pan, 2}, {queso, 1}},
        72.5
    };

    int accion = 0, comida = 0;

    while (true) {
        printf("\nElija una accion: \n");
        printf("1) Vender un plato.\n");
        printf("2) Consultar Stock Critico.\n");
        printf("3) Recargar Stock.\n");
        printf("\nEleccion (Ctrl+C para salir): ");
        scanf("%d", &accion);

        switch (accion) {
            case 1:
                printf("\nSeleccione un plato, disponibles hoy: \n");
                printf("1) Hamburguesa.\n");
                printf("\nEleccion: ");
                scanf("%d", &comida);

                Receta * plato;

                if (comida == 1) {
                    plato = &hamburguesa;
                } else {
                    printf("Opcion no valida!\n");
                    break;
                }

                if (chequearStock(*plato)) {
                    printf("A cobrar: $%4.2f\n", (*plato).precio);
                    cocinar(*plato);
                }
            break;

            case 2:
                for (int i = 0; i < 4; i++) {
                    if (stockCritico((*listaIngredientes[i]))) {
                        printf("El stock de %s esta por debajo del 30%% (%d/%d).\n", (*listaIngredientes[i]).nombre.c_str(), (*listaIngredientes[i]).stock, (*listaIngredientes[i]).stockMaximo);
                    } else {
                        printf("El stock de %s es normal (%d/%d).\n", (*listaIngredientes[i]).nombre.c_str(), (*listaIngredientes[i]).stock, (*listaIngredientes[i]).stockMaximo);
                    }
                }
            break;

            case 3:
                for (int i = 0; i < 4; i++) {
                    if ((*listaIngredientes[i]).stock < (*listaIngredientes[i]).stockMaximo) {
                        string eleccion;
                        printf("Desea recargar el stock de %s (s/n): ", (*listaIngredientes[i]).nombre.c_str());
                        cin >> eleccion;

                        if (eleccion == "s") {
                            int cantidad = 0;
                            printf("Ingrese la cantidad a recargar: ");
                            scanf("%d", &cantidad);

                            if (cantidad > 0 && (*listaIngredientes[i]).stock + cantidad <= (*listaIngredientes[i]).stockMaximo) {
                                recargar((*listaIngredientes[i]), cantidad);
                                printf("El stock de %s es (%d/%d).\n", (*listaIngredientes[i]).nombre.c_str(), (*listaIngredientes[i]).stock, (*listaIngredientes[i]).stockMaximo);
                            } else {
                                printf("El valor ingresado no es valido o supera el stock maximo.\n");
                            }
                        } else if (eleccion != "n") {
                            printf("Opcion no valida!\n");
                        }
                    } else {
                        printf("No es posible aumentar el stock de %s.\n", (*listaIngredientes[i]).nombre.c_str());
                    }
                }
            break;

            default:
            break;
        }
    }
    return 0;
}