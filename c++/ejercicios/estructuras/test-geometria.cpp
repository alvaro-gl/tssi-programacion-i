#include <iostream>
#include "../../test/catch.hpp"
#include "estructuras.h"
#include "funciones.h"
using namespace std;

bool checkArray (float first[], float segundo[], int cantidad) {
    bool equalArrays = true;

    for (int i = 0; i < cantidad; i++) {
        //printf("%d - %d\n", first[i], segundo[i]);
        if (first[i] != segundo[i]) {
            equalArrays = false;
        }
    }

    return equalArrays;
}

TEST_CASE( "Distancia entre dos puntos - misma posicion" ) {
    Punto posInicial = {1, 1};
    
    REQUIRE( distancia(posInicial, posInicial) == 0 );
}

TEST_CASE( "Distancia entre dos puntos - distinta posicion" ) {
    Punto posInicial = {1, 1};
    Punto posFinal = {5, 4};

    REQUIRE( distancia(posFinal, posInicial) == 5);
}

TEST_CASE( "Recta que pasa por dos puntos" ) {
    Punto posInicial = {2, 1};
    Punto posFinal = {-1, -5};

    float pendiente, ordenada;

    ecuacionRectaDosPuntos(posInicial, posFinal, pendiente, ordenada);

    REQUIRE( pendiente == float(2) );
    REQUIRE( ordenada == float(-3) );
}

TEST_CASE( "Cinco puntos de la recta" ) {
    Punto posInicial = {2, 1};
    Punto posFinal = {-1, -5};

    float fixture[5] = {-3, -1, 1, 3, 5};
    float r[5];

    cincoPuntos(posInicial, posFinal, r);

    bool equalArrays = checkArray(fixture, r, 5);

    REQUIRE( equalArrays );
}