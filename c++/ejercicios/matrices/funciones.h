#define MAX_FILAS 30
#define MAX_COLUMNAS 25

void obtenerColumna (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int columna, int r[]);
float promedio(int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols);
void genSumaColumna (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]);
void maxCadaFila (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]);
void obtenerDiagonalPal (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]);
void obtenerDiagonalNoPal(int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]);
int sumatoriaDiagonalPal(int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols);
int sumatoriaDiagonalNoPal(int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols);
void maxCuarto (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]);
void obtenerTriangularSup (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]);
void obtenerTriangularInf (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]);