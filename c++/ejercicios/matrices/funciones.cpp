#include <iostream>
using namespace std;

#define MAX_FILAS 30
#define MAX_COLUMNAS 25

int sumadorArray (int r[], int rows) {
    int contador = 0;

    for (int i = 0; i < rows; i++){
        contador += r[i];
    }

    return contador;
}

void obtenerColumna (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int columna, int r[]) {
    for (int i = 0; i < rows; i++) {
        r[i] = matriz[i][columna];
    }
}

float promedio(int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols) {
    int r = 0;
    int contador = 0;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            r += matriz[i][j];
            contador += 1;
        }
    }

    return float(r) / float(contador);
}

void genSumaColumna (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    for (int i = 0; i < cols; i++) {
        int ir[cols];
        int contador = 0;
        
        obtenerColumna(matriz, rows, cols, i, ir);

        for (int j = 0; j < rows; j++) {
            contador += ir[j];
        }

        r[i] = contador;
    }
}

void maxCadaFila (int matriz[MAX_FILAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    for (int i = 0; i < rows; i++) {
        int max = 0;

        for (int j = 0; j < cols; j++) {
            if (matriz[i][j] > max) {
                max = matriz[i][j];
            }
        }

        r[i] = max;
    }
}

// Voy a seguir pasando rows y cols para que sea mas uniforme
void obtenerDiagonalPal (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (i == j) {
                r[i] = matriz[i][j];
            }
        }
    }
}

void obtenerDiagonalNoPal (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    int pointer = cols - 1;

    for (int i = 0; i < rows; i++) {
        r[i] = matriz[i][pointer];
        pointer -= 1;
    }
}

int sumatoriaDiagonalPal (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols) {
    int r[MAX_COLUMNAS];
    int contador = 0;

    obtenerDiagonalPal(matriz, rows, cols, r);

    for (int i = 0; i < rows; i++) {
        contador += r[i];
    }

    return contador;
}

int sumatoriaDiagonalNoPal (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols) {
    int r[MAX_COLUMNAS];
    int contador = 0;

    obtenerDiagonalNoPal(matriz, rows, cols, r);

    for (int i = 0; i < rows; i++) {
        contador += r[i];
    }

    return contador;
}

void maxCuarto (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    int q0[rows], q1[rows], q2[rows], q3[rows];
    int c0 = 0, c1 = 0, c2 = 0, c3 = 0;
    int p0 = 0, p1 = 0, p2 = 0, p3 = 0;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {

            if (i < rows / 2 && j < cols / 2) {
                q0[p0] = matriz[i][j];
                p0 += 1;
            }

            if (i < rows / 2 && j >= cols / 2) {
                q1[p1] = matriz[i][j];
                p1 += 1;
            }

            if (i >= rows / 2 && j < cols / 2) {
                q2[p2] = matriz[i][j];
                p2 += 1;
            }

            if (i >= rows / 2 && j >= cols / 2) {
                q3[p3] = matriz[i][j];
                p3 += 1;
            }
        }
    }

    c0 = sumadorArray(q0, rows);
    c1 = sumadorArray(q1, rows);
    c2 = sumadorArray(q2, rows);
    c3 = sumadorArray(q3, rows);

    if (c0 > c1 && c0 > c2 && c0 > c3) {
        for (int i = 0; i < rows; i++) {
            r[i] = q0[i];
        }
    }

    if (c1 > c0 && c1 > c2 && c1 > c3) {
        for (int i = 0; i < rows; i++) {
            r[i] = q1[i];
        }
    }

    if (c2 > c0 && c2 > c1 && c2 > c3) {
        for (int i = 0; i < rows; i++) {
            r[i] = q2[i];
        }
    }

    if (c3 > c0 && c3 > c1 && c3 > c2) {
        for (int i = 0; i < rows; i++) {
            r[i] = q3[i];
        }
    }
}

void obtenerTriangularSup (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    int puntero = 0;

    for (int i = 0; i < (rows - 1); i++) {
        for (int j = i + 1; j < cols; j++) {
            r[puntero] = matriz[i][j];
            puntero += 1;
        }
    }
}

void obtenerTriangularInf (int matriz[MAX_COLUMNAS][MAX_COLUMNAS], int rows, int cols, int r[]) {
    int puntero = 0;

    for (int i = 1; i < rows; i++) {
        for (int j = 0; j < i; j++) {
            r[puntero] = matriz[i][j];
            puntero += 1;
        }
    }
}