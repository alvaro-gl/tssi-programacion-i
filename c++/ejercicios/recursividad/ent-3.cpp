#include <iostream>
#include <vector>
#include "funciones.h"
using namespace std;

/* La secuencia de Collatz de un número entero se construye de la siguiente forma: 
  - Si el número es par, se lo divide por dos
  - Si es impar, se le multiplica tres y se le suma uno 
  - La sucesión termina al llegar a uno.

La conjetura de Collatz afirma que, al partir desde cualquier número, la secuencia siempre llegará a 1. 
A pesar de ser una afirmación a simple vista muy simple, no se ha podido demostrar si es cierta o no.
Desarrolle un programa que entregue la secuencia de Collatz de un número entero.
Ejemplo Para n = 18, la conjetura de Collatz sería:
18 9 28 14 7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1 */


int main() {
    int valor = 0;
    std::vector<int> coll;

    while (valor < 1) {
        printf("Ingrese un valor para realizar la serie de Collatz: ");
        scanf("%d", &valor);
    }

    printf("La conjetura de Collatz para %d es: ", valor);
    collatz(valor, coll);

    for (int i = 0; i < coll.size(); i++) {
        printf(" %d ", coll.at(i));
    }

    printf("\n");

    return 0;
}