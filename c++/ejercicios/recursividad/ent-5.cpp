#include <iostream>
#include "funciones.h"
using namespace std;

/* La descripcion era demasiado larga:
   Link de Wikipedia del PDF: https://es.wikipedia.org/wiki/Sistema_octal#M%C3%A9todos_de_conversi%C3%B3n
*/

int main() {
    int valor = 0;

    printf("Ingrese un valor decimal para calcular su octal: ");
    scanf("%d", &valor);

    printf("El resultado es: %d\n", decimalToOctal(valor));
    return 0;
}