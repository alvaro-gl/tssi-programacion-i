#include <iostream>
#include "funciones.h"
using namespace std;

/* Desarrollar una función recursiva que permita dada la cantidad de términos generar la siguiente serie:
N = 5 ==> {0, 1, 3, 6, 10, 15} */

int main() {
    int valor = 0;

    printf("Ingrese un valor para calcular la serie: ");
    scanf("%d", &valor);

    printf("El resultado es %d\n", serieL(valor));
    return 0;
}