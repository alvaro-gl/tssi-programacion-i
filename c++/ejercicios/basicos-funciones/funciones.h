#include <vector>

char posNeg(int valor);

float celsiusAFahrenheit(float temperatura);

bool entreCeroYNueve(int valor);

bool esVocal(char letra);

int mayor(int numeroA, int numeroB);

void nPrimos(std::vector<int>& primos, int cantidad);