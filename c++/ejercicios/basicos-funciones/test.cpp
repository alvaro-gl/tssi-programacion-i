#include <iostream>
#include <vector>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

TEST_CASE( "Valor positivo para posNeg" ) {
    REQUIRE( posNeg(35) == 'P' );
}

TEST_CASE( "Valor negativo para posNeg" ) {
    REQUIRE( posNeg(-4) == 'N' );
}

TEST_CASE( "Conversion Celsius a Fahr" ) {
    REQUIRE( celsiusAFahrenheit(20) == 68.0 );
}

TEST_CASE( "Entre 0 y 9 - True" ){
    REQUIRE( entreCeroYNueve(5) == true );
}

TEST_CASE( "Entre 0 y 9 - False" ){
    REQUIRE( entreCeroYNueve(19) == false );
}

TEST_CASE( "Es vocal mayuscula - True" ){
    REQUIRE( esVocal('A') == true );
}

TEST_CASE( "Es vocal minuscula - True" ){
    REQUIRE( esVocal('u') == true );
}

TEST_CASE( "Es vocal mayuscula - False" ){
    REQUIRE( esVocal('Z') == false );
}

TEST_CASE( "Es vocal minuscula - False" ){
    REQUIRE( esVocal('b') == false );
}

TEST_CASE( "Es mayo el primero" ){
    REQUIRE( mayor(50, 10) == 50 );
}

TEST_CASE( "Es mayo el segundo" ){
    REQUIRE( mayor(50, 100) == 100 );
}

TEST_CASE( "Pido los primeros 10 primos" ){
    vector<int> pv{2,3,5,7,11,13,17,19,23,29};
    vector<int> tv;

    nPrimos(tv, 10);

    CHECK_THAT( tv, Catch::Equals(pv) );
}