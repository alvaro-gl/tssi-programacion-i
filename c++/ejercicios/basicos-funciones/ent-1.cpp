#include <iostream>
#include <vector>
#include "funciones.h"
using namespace std;

/* Ejercicio a presentar durante la clase de 2020-04-23 */

int main() {

    int cantidad = 0;

    printf("Llamando al ejercicio 1 con 5 como valor: %c\n", posNeg(5));
    printf("Llamando al ejercicio 1 con -18 como valor: %c\n", posNeg(-18));
    printf("Llamando al ejercicio 2 con 27.5º como valor: %4.2f\n", celsiusAFahrenheit(27.5));
    // StackOverflow intensifies con el siguiente printf
    printf("Llamando al ejercicio 3 con 5 como valor: %s\n", entreCeroYNueve(5)? "True": "False");
    printf("Llamando al ejercicio 3 con 99 como valor: %s\n", entreCeroYNueve(99)? "True": "False");
    printf("Llamando al ejercicio 4 con la letra A: %s\n", esVocal('A')? "True": "False");
    printf("Llamando al ejercicio 4 con la letra T: %s\n", esVocal('T')? "True": "False");
    printf("Llamando al ejercicio 5 con 54 y 45 como valores: %d\n", mayor(54, 45));
    printf("Llamando al ejercicio 5 con 93 y 5 como valores: %d\n", mayor(93, 5));
    printf("Llamando al ejercicio 6, voy a pedirle los primeros 15 numeros primos\n");

    cantidad = 15;
    vector<int> primos;

    nPrimos(primos, cantidad);
    printf("Los primeros %d numeros primos son: ", cantidad);

    for (int i = 0; i < cantidad; i++) {
        printf("%d ", primos.at(i));
    }

    printf("\n");

    return 0;
}