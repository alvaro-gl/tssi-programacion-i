#include <iostream>
#include "estructuras.h"
using namespace std;

const string apellidos[10] = {
    "Perez", "Garcia", "Fredes", "Penia", "Sepia",
    "Macri", "Lanato", "Baez", "Lopez", "Marquez"
};

const int legajos[10] = {
    12559665, 13304019, 17435978, 19705451, 20801669,
    21507758, 22145213, 28630713, 33712576, 51407468
};

void generarAlumnos (Alumno a[10]) {
    for (int i = 0; i < 10; i++) {
        a[i] = {legajos[i], apellidos[i]};
    }
}

void generarExamenes (Alumno a[10], Examen ex[50]) {
    srand(time(0));

    for (int i = 0; i < 50; i++) {
        int alumno = rand() % 11;
        int nota = rand() % 11;

        ex[i] = {a[alumno], nota};
    }
}

void ordenarPorLegajo (Examen ex[], int ce) {
    int i = 0;
    int j;
    Examen aux;
    bool ordenado = false;

    while (i < ce && !ordenado) {
        ordenado = true;

        for (j = 0; j < ce - i - 1; j++) {
            if (ex[j].alum.legajo > ex[j + 1].alum.legajo) {
                aux = ex[j];
                ex[j] = ex[j + 1];
                ex[j + 1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

void corte (Examen ex[], int ce) {
    int i  = 0;
    int key;

    while (i < ce) {
        key = ex[i].alum.legajo;
        string nombre = ex[i].alum.nombre;
        int suma = 0;
        int contador = 0;

        while (i < ce && ex[i].alum.legajo == key) {
            suma += ex[i].nota;
            contador += 1;
            i++;
        }

        printf("El promedio de notas para %s es %4.2f\n", nombre.c_str(), float(suma) / float(contador));
    }
}

void imprimir (int alpha[], int ca) {
    printf("Vector resultado: \n");
    for (int i = 0; i < ca; i++) {
        printf(" %d ", alpha[i]);
    }
    printf("\n");
}

// Apareo normal para vectores ordenados sin valores repetidos
void apareoNormal (int alpha[], int ca, int beta[], int cb, int gamma[], int& cc) {
    int i = 0, j = 0;

    // Se supone que nos pasan cc inicializado con 0.
    while (i < ca && j < cb) {
        if (alpha[i] < beta[j]) {
            gamma[cc] = alpha[i];
            i++;
        } else {
            gamma[cc] = beta[j];
            j++;
        }
        cc++;
    }

    while (i < ca) {
        gamma[cc] = alpha[i];
        i++;
        cc++;
    }

    while (j < cb) {
        gamma[cc] = beta[j];
        j++;
        cc++;
    }
}

void burbujeo (int alpha[], int ca) {
    int i = 0;
    int j, aux;
    bool ordenado = false;

    while (i < ca && !ordenado) {
        ordenado = true;

        for (j = 0; j < ca - i -1; j++) {
            if (alpha[j] > alpha[j + 1]) {
                aux = alpha[j];
                alpha[j] = alpha[j + 1];
                alpha[j + 1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

int binaria (int alpha[], int cant, int b) {
    int inicio = 0;
    int fin = cant - 1;

    while (fin >= inicio) {
        int mitad = inicio + (fin - inicio) / 2;

        // Si está en el medio devuelvo la pos
        if (alpha[mitad] == b) {
            return mitad;
        }

        if (b > alpha[mitad]) {
            inicio = mitad + 1;
        } else {
            fin = mitad - 1;
        }
    }

    return -1;
}

void insertion (int alpha[], int ca) {
    int i, j, key;

    for (i = 1; i < ca; i++) {
        key = alpha[i];
        // j arranca en 0;
        j = i - 1;

        while (j >= 0 && alpha[j] > key) {
            alpha[j + 1] = alpha[j];
            j = j - 1;
        }
        alpha[j + 1] = key;
    }
}