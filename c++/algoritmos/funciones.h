#include "estructuras.h"

void imprimir (int alpha[], int ca);
void apareoNormal (int alpha[], int ca, int beta[], int cb, int gamma[], int& cc);
void burbujeo (int alpha[], int ca);
int binaria (int alpha[], int cant, int b);
void insertion (int alpha[], int ca);
void generarAlumnos (Alumno a[10]);
void generarExamenes (Alumno a[10], Examen ex[50]);
void ordenarPorLegajo (Examen ex[], int ce);
void corte (Examen ex[], int ce);