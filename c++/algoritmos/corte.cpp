#include <iostream>
#include "funciones.h"
using namespace std;

int main () {
    Alumno a[10];
    generarAlumnos(a);

    Examen examenes[50];
    generarExamenes(a, examenes);

    printf("==== Alumnos\n");
    for (int i = 0; i < 10; i++) {
        printf("Legajo: %d - Nombre: %s\n", a[i].legajo, a[i].nombre.c_str());
    }

    printf("==== Notas\n");
    for (int i = 0; i < 50; i++) {
        printf("Legajo: %d - Nombre: %s - Nota: %d\n", examenes[i].alum.legajo, examenes[i].alum.nombre.c_str(), examenes[i].nota);
    }

    ordenarPorLegajo(examenes, 50);

    printf("===== Notas Ordenadas por legajo\n");
    for (int i = 0; i < 50; i++) {
        printf("Legajo: %d - Nombre: %s - Nota: %d\n", examenes[i].alum.legajo, examenes[i].alum.nombre.c_str(), examenes[i].nota);
    }

    corte(examenes, 50);

    return 0;
}