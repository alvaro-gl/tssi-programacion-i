#include <iostream>
#include "funciones.h"
using namespace std;

int main () {
    int alpha[5] = {5, 3, 1, 6, 2};

    // === Primer ciclo ===
    // i = 1; => alpha[1] => 3
    // key = 3;
    // j = 0; => alpha[0] => 5

    // Condiciones del while
    // j >= 0? Si, alpha[0] > key? Si (5 > 3)
    // alpha[j+1] = alpha[j]; => alpha[1] = 5
    // j = j - 1; => -1
    // Queda [5 5 1 6 2]

    // Afuera del while
    // alpha[j + 1] = key; alpha[0] = key => 3
    // Queda [3 5 1 6 2]

    // === Segundo ciclo ===
    // i = 2; => alpha[2] => 1
    // key = alpha[2] => 1;
    // j = 1; => alpha[1] => 5

    // Condiciones del while
    // j >= 0? Si (1 > 0), alpha[1] > key? Si (5 > 1)
    // alpha[j+1] = alpha[j] => alpha[2] = 5
    // j = j - 1 => 0
    // Queda [3 5 5 6 2]

    // Condiciones del while
    // j >= 0? Si (0 >= 0), alpha[0] > key? Si (3 > 1)
    // alpha[j+1] = alpha[j] = alpha[1] = 3
    // j = j - 1 => -1
    // Queda [3 3 5 6 2]

    // Afuera del while
    // alpha[j + 1] = alpha[0] = key => 1
    // Queda [1 3 5 6 2]

    insertion(alpha, 5);

    imprimir(alpha, 5);

    return 0;
}
