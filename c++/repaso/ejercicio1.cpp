#include <iostream>
#include <time.h>

using namespace std;

void vectorPrimos (int r[], int cantidad) {
    int n;
    srand(time(NULL));

    int k = 0;
    while (k < cantidad){

        n = rand() % 1000 + 1;
        int x = 2;
        for (int i = 2; i < n; i++)
        {
            if (n % i == 0){
                x++;
            }
        }

        bool esP = true;
        if (x > 2){
            esP = false;
        }
        
        if(esP){
            r[k] = n;
            k++;
        }
    }
}

void ordenarVector (int r[], int cantidad) {
    int aux = 0;

    for (int i = 0; i < cantidad - 1; i++) {
        for (int j = 0; j < cantidad - i - 1; j++) {
            if (r[j] <= r[j + 1]) {
                aux = r[j];
                r[j] = r[j + 1];
                r[j + 1] = aux;
            }
        }
    }
}

void imprimirVector (int r[], int cantidad) {
    for (int i = 0; i < cantidad; i++) {
        cout << "Numero: " << r[i] << endl;
    }
}

int main () {
    int p[100];
    vectorPrimos(p, 100);

    if (p[0] % 2 != 0 && p[0] < 10){
        cout << "Vector Original: " << endl;
        imprimirVector(p, 100);

        ordenarVector(p, 100);

        cout << "Nuevo vector: " << endl;
        imprimirVector(p, 100);
        
    }
    else {
        cout << "Vector Original: " << endl;
        imprimirVector(p, 100);
    }
    return 0;
}
