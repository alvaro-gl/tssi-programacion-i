#include <iostream>
#include <random>
#include <algorithm>
#define MAX_OBRAS 10
#define MAX_CATEGORIAS 3
using namespace std;

const string autoresObrasExistentes[MAX_OBRAS][2] = {
    {"Leonardo Da Vinci", "Mona Lisa"},
    {"Miguel Angel", "Vaticano"},
    {"Vincent Van Gogh", "Lirios"},
    {"Gentileschi", "Judith y su doncella"},
    {"Rembrandt", "El jinete polaco"},
    {"Turner", "Cuadrazo"},
    {"Vincent Van Gogh", "La noche estrellada"},
    {"Zonet", "Amapolas"},
    {"Degas", "La clase de ballet"},
    {"Picasso", "Guernica"}
};

const string autoresObrasNuevas[MAX_OBRAS][2] = {
    {"Leonardo Da Vinci", "La ultima cena"},
    {"Miguel Angel", "El juicio final"},
    {"Vincent Van Gogh", "El cafe de noche"},
    {"Rembrandt", "El festin de Baltasar"},
    {"Turner", "Twitazo"},
    {"Alejandro", "Magno"},
    {"El dipy", "Cha cha"},
    {"Gustav Klimt", "El beso"},
    {"Picasso", "La mujer que llora"},
    {"Picasso", "El suenio"}
};

struct categoria {
    int codigo;
    string nombre;
};

struct obra {
    int codigo;
    string nombre;
    string autor;
    categoria cat;
};

const categoria cats[3] = {
    {100, "Renacentismo"},
    {200, "Figurativo"},
    {300, "Cubismo"}
};

void generarObras (obra ob[MAX_OBRAS], const string dataset[MAX_OBRAS][2]) {
    for (int i = 0; i < MAX_OBRAS; i++) {
        
        random_device rd;
        mt19937 generator(rd());
        uniform_int_distribution<int> distribution(0, 2);

        int n = distribution(generator);

        ob[i].cat = cats[n];
        ob[i].autor = dataset[i][0];
        ob[i].nombre = dataset[i][1];
        ob[i].codigo = rand() % 1000 + 1;        
    }
}

void unificarConjuntos (obra ob1[], int c1, obra ob2[], int c2, obra r[], int& cantidad) {
    int i = 0, j = 0, k = 0;

    while (i < c1 && j < c2) {
        if (ob1[i].codigo < ob2[j].codigo) {
            r[k] = ob1[i];
            i++;
            k++;
        } else {
            r[k] = ob2[j];
            j++;
            k++;
        }
    }

    if (i < c1) {
        while (i < c1) {
            r[k] = ob1[i];
            i++;
            k++;
        }
    }

    if (j < c2) {
        while (j < c2) {
            r[k] = ob2[j];
            j++;
            k++;
        }
    }
}

// Ordena por codigo de obra
void ordenarPorCodigo (obra ob[], int cantidad) {
    obra aux;

    for (int i = 0; i < cantidad - 1; i++) {
        for (int j = 0; j < cantidad - i - 1; j++) {
            if (ob[j].codigo >= ob[j + 1].codigo) {
                aux = ob[j];
                ob[j] = ob[j + 1];
                ob[j + 1] = aux;
            }
        }
    }
}

// Ordenar por codigo de categoria 
void ordenarPorCategoria (obra ob[], int cantidad) {
    obra aux;

    for (int i = 0; i < cantidad - 1; i++) {
        for (int j = 0; j < cantidad - i - 1; j++) {
            if (ob[j].cat.codigo >= ob[j + 1].cat.codigo) {
                aux = ob[j];
                ob[j] = ob[j + 1];
                ob[j + 1] = aux;
            }
        }
    }
}

// Ordenar por autor cuando ya esta ordenado por categoria
void ordenarPorAutor (obra ob[], int cantidad) {
    obra aux;

    for (int i = 0; i < cantidad - 1; i++) {
        for (int j = 0; j < cantidad - i - 1; j++) {
            if (ob[j].cat.codigo == ob[j + 1].cat.codigo && ob[j].autor.at(0) >= ob[j + 1].autor.at(0)) {
                aux = ob[j];
                ob[j] = ob[j + 1];
                ob[j + 1] = aux;
            }
        }
    }
}

int contarObrasPorCategoria (obra ob[], int cantidad, string autor, int codCat) {
    int n = 0;

    for (int i = 0; i < cantidad; i++) {
        if (ob[i].autor == autor && ob[i].cat.codigo == codCat) {
            n++;
        }
    }
    
    return n;
}

void imprimir (obra ob[], int cantidad) {
    for (int i = 0; i < cantidad; i++) {
        printf("Nombre: %s \nCodigo: %d \nCategoria: %d\nAutor: %s\n---\n", ob[i].nombre.c_str(), ob[i].codigo, ob[i].cat.codigo, ob[i].autor.c_str());
    }
}

void imprimirPorCategoria(obra ob[], int cantidad) {
    for (int i = 0; i < MAX_CATEGORIAS; i++) {
        printf("Categoria: %s\n", cats[i].nombre.c_str());
        for (int j = 0; j < cantidad; j++) {
            if (ob[j].cat.codigo == cats[i].codigo) {
                printf("  %s - %s\n", ob[j].nombre.c_str(), ob[j].autor.c_str());
            }
        }
    }
}

void imprimirPorCategoriaYAutor (obra ob[], int cantidad) {
    int c = 0;
    for (int i = 0; i < MAX_CATEGORIAS; i++) {
        printf("Categoria: %s\n", cats[i].nombre.c_str());

        for (int j = c; j < cantidad; j++) {
            int n = contarObrasPorCategoria(ob, cantidad, ob[j].autor, cats[i].codigo);
            if (n > 0) {
                printf("  Autor: %s - Cantidad: %d\n", ob[j].autor.c_str(), n);
            }
            if (n > 1) {
                j += n - 1;
                c = j;
                if (ob[j].cat.codigo != ob[j-1].cat.codigo) {
                    break;
                }
            }
            if (ob[j+1].cat.codigo != ob[j].cat.codigo) {
                c = j + 1;
                break;
            }
            c = j;
        }
    }
}

int main () {
    obra existentes[MAX_OBRAS];
    obra nuevas[MAX_OBRAS];

    generarObras(existentes, autoresObrasExistentes);
    generarObras(nuevas, autoresObrasNuevas);

    ordenarPorCodigo(existentes, MAX_OBRAS);
    ordenarPorCodigo(nuevas, MAX_OBRAS);

    // Punto 1
    int cantidad = MAX_OBRAS * 2;
    // Si no le pongo la cantidad me devuelve un error de incomplete type not allowed
    obra r[cantidad];
    unificarConjuntos(existentes, MAX_OBRAS, nuevas, MAX_OBRAS, r, cantidad);

    // Punto 2
    printf("\nConjunto completo ordenado por codigo de categoria:\n");
    printf("---------------------------------------------------\n");
    ordenarPorCategoria(r, cantidad);
    imprimirPorCategoria(r, cantidad);

    // Punto 3
    ordenarPorAutor(r, cantidad);
    printf("\nOrdenado por Categoria y luego Autor:\n");
    printf("-------------------------------------\n");
    imprimirPorCategoriaYAutor(r, cantidad);
    return 0;
}