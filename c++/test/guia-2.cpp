#include <iostream>
#include "catch.hpp"
using namespace std;

int fecha (int dia, int mes, int anio) {
	return anio * 10000 + mes * 100 + dia;
}

TEST_CASE ( "fecha-1990" ) {
	REQUIRE( fecha(26, 8, 1989) == 19890826 );
}

TEST_CASE ( "fecha-2000" ) {
	REQUIRE( fecha(1, 8, 2000) == 20000801 );
}

TEST_CASE ( "fecha-2020" ) {
	REQUIRE( fecha(1, 8, 2020) == 20200801 );
}
