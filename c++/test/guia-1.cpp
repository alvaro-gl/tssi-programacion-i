#include <iostream>
#include "catch.hpp"
using namespace std;


int suma (int a, int b) {
    return a + b;
}

int resta (int a, int b) {
    return a - b;
}

int producto (int a, int b) {
    return a * b;
}

TEST_CASE( "suma" ) {
    REQUIRE( suma(2, 2) == 4 );
}

TEST_CASE( "resta" ) {
    REQUIRE( resta(2, 2) == 0 );
}

TEST_CASE( "producto" ) {
    REQUIRE( producto(3, 2) == 6 );
}
