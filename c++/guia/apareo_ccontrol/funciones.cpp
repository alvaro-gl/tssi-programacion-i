#include <random>
#include <math.h>
#include "estructuras.h"

#define ANIO_INICIAL 2018
#define ANIO_FINAL 2020

const string apellidos[10] = {
    "Perez", "Garcia", "Fredes", "Penia", "Sepia", 
    "Macri", "Lanato", "Baez", "Lopez", "Marquez"
};

const int materias[5] = {
    133891, 267544, 874175, 744971, 488406
};

const int legajos[10] = {
    12559665, 13304019, 17435978, 19705451, 20801669,
    21507758, 22145213, 28630713, 33712576, 51407468
};

bool esMayor (Persona &a, Persona &b) {
    bool r;

    if (a.fecNac.anio < b.fecNac.anio) {
        r = true;
    } else if (a.fecNac.anio == b.fecNac.anio) {
        if (a.fecNac.mes < b.fecNac.mes) {
            r = true;
        } else if (a.fecNac.mes == b.fecNac.mes) {
            if (a.fecNac.dia < b.fecNac.dia) {
                r = true;
            } else {
                r = false;
            }
        } else {
            r = false;
        }
    } else {
        r = false;
    }

    return r;
}

void ordenarPorFechaNacimiento (Persona p[], int cantidad) {
    int i = 0, j;
    Persona aux;
    bool ordenado = false;

    while (i < cantidad && !ordenado) {
        ordenado = true;

        for (j = 0; j < cantidad-i-1; j++) {
            if (esMayor(p[j], p[j+1])) {
                aux = p[j];
                p[j] = p[j+1];
                p[j+1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

int getNumeroRandomPorDigitos (int digitos) {
    int inicio = pow(10, digitos - 1);
    int final = (inicio * 10) - 1; 

    random_device rd;
    mt19937 generator(rd());
    uniform_int_distribution<int> distribution(inicio, final);

    return distribution(generator);
}

int getNumeroRandomPorRango (int inicio, int final) {
    random_device rd;
    mt19937 generator(rd());
    uniform_int_distribution<int> distribution(inicio, final);

    return distribution(generator);
}

int getDiaRandom (int mes) {
    const int inicio = 1;
    int r = 0;
    random_device rd;
    mt19937 generator(rd());

    if (mes == 2) {
        uniform_int_distribution<int> distribution(inicio, 28);
        r = distribution(generator);
    } else if (mes == 4 || mes == 6 || mes == 7 | mes == 11) {
        uniform_int_distribution<int> distribution(inicio, 30);
        r = distribution(generator);
    } else {
        uniform_int_distribution<int> distribution(inicio, 31);
        r = distribution(generator);
    }

    return r;
}

void generarBoletasMayo (Boleta boletas[], int cantidad) {
    for (int i = 0; i < cantidad; i++) {
        boletas[i].anio = 2020;
        boletas[i].mes = 5;
        boletas[i].dia = getDiaRandom(5);
        boletas[i].apellido = apellidos[getNumeroRandomPorRango(0,9)];
        boletas[i].codigo_materia = materias[getNumeroRandomPorRango(0,4)];
        boletas[i].legajo = legajos[getNumeroRandomPorRango(0,9)];
    }
}

void generarBoletasPreviasMayo (Boleta boletas[], int cantidad) {
    for (int i = 0; i < cantidad; i++) {
        boletas[i].anio = getNumeroRandomPorRango(ANIO_INICIAL, ANIO_FINAL);
        int mes = getNumeroRandomPorRango(1,4);
        boletas[i].mes = mes;
        boletas[i].dia = getDiaRandom(mes);
        boletas[i].apellido = apellidos[getNumeroRandomPorRango(0,9)];
        boletas[i].codigo_materia = materias[getNumeroRandomPorRango(0,4)];
        boletas[i].legajo = legajos[getNumeroRandomPorRango(0,9)];
    }
}

void generarBoletas (Boleta boletas[], int cantidad) {
    for (int i = 0; i < cantidad; i++) {
        boletas[i].anio = getNumeroRandomPorRango(ANIO_INICIAL, ANIO_FINAL);
        int mes = getNumeroRandomPorRango(1,12);
        boletas[i].mes = mes;
        boletas[i].dia = getDiaRandom(mes);
        boletas[i].apellido = apellidos[getNumeroRandomPorRango(0,9)];
        boletas[i].codigo_materia = materias[getNumeroRandomPorRango(0,4)];
        boletas[i].legajo = getNumeroRandomPorDigitos(8);
    }
}

void imprimirInscripcionPorMateria (Boleta b[], int cantidad, int codigo_materia) {
    printf("Legajo  \tApellido\tFecha      \tCodigo de materia\n");
    printf("--------\t--------\t-----------\t-----------------\n");
    for (int i = 0; i < cantidad; i++) {
        if (b[i].codigo_materia == codigo_materia) {
            printf("%d\t%s\t\t%d/%d/%d\t%d\n", b[i].legajo, b[i].apellido.c_str(), b[i].dia, b[i].mes, b[i].anio, b[i].codigo_materia);
        }
    }
    printf("\n");
}

void anexarBoletas(Boleta a[], int cantA, Boleta b[], int cantB, Boleta r[]) {
    for (int i = 0; i < cantA; i++) {
        r[i] = a[i];
    }

    for (int i = 0; i < cantB; i++) {
        r[cantA + i] = b[i];
    }
}

void ordenarBoletasPorLegajo (Boleta a[], int cantidad) {
    int i = 0;
    int j;
    Boleta aux;
    bool ordenado = false;

    while (i < cantidad && !ordenado) {
        ordenado = true;

        for (j = 0; j < cantidad-i-1; j++) {
            if (a[j].legajo > a[j+1].legajo) {
                aux = a[j];
                a[j] = a[j+1];
                a[j+1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

void listarConDosExamenes (Boleta a[], int cantidad) {
    int i = 0;
    int inscripciones;
    int key;

    while (i < cantidad) {
        key = a[i].legajo;
        inscripciones = 0;

        while (i < cantidad && a[i].legajo == key) {
            inscripciones++;
            i++;
        }
        printf("El legajo %d tiene %d inscripciones\n", a[i].legajo, inscripciones);
    }
}