#include <iostream>
using namespace std;

/* Dados un mes y el año correspondiente informar cuántos días tiene el mes. */


int main() {

    int anio, mes, diasFebrero;

    cout << "Ingrese un anio: ";
    cin >> anio;

    cout << "Ingrese un mes: ";
    cin >> mes;

    if ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0) {
        diasFebrero = 29;
    } else {
        diasFebrero = 28;
    }

    if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
        printf("El mes tiene %2d dias.\n", 31);
    } else if (mes == 2){
        printf("El mes tiene %2d dias.\n", diasFebrero);
    } else {
        printf("El mes tiene %2d dias.\n", 30);
    } 

    return 0;
}