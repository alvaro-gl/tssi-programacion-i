#include <iostream>
using namespace std;

// Dados dos valores enteros A y B, informar la suma, la resta y el producto.

int main() {
    int a, b;

    cout << "Ingrese el primer numero: ";
    cin >> a;

    cout << "Ingrese el segundo numero: ";
    cin >> b;

    cout << "La suma es: " << a + b << "\n";
    cout << "La resta es: " << a - b << "\n";
    cout << "El producto es: " << a * b << "\n";

    return 0;
}
