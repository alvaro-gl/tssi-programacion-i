#include <iostream>
using namespace std;

/* Se realiza una inspección en una fábrica de pinturas, y se detectaron 20 infracciones​. ​ 
   De cada infracción se tomó nota de los siguientes datos:
     - Tipo de Infracción (1, 2, 3 o 4)
     - Motivo de la infracción
     - Valor de la multa
     - Gravedad de la infracción (‘L’,‘M’, ‘G’)
   Se pide informar al final del proceso:
     -​ Los valores totales de la multa a pagar de acuerdo al tipo de gravedad.
     -​ La leyenda “Clausurar fábrica” si la cantidad de infracciones 3 y 4 con gravedad “G” sean mayor a 3. */


int main() {
    const int valorMultaL = 100;
    const int valorMultaM = 500;
    const int valorMultaG = 1000;

    int cantInfr1 = 0, cantInfr2 = 0, cantInfr3 = 0, cantInfr4 = 0;
    int vTotalL = 0, vTotalM = 0, vTotalG = 0;
    int totalInfG34 = 0;

    for (int i = 0; i < 5; i++) {
      int tipoInfraccion = 0;
      char gravedadInfraccion = ' ';
      string motivo = "";
  
      cout << "Ingrese el tipo de infraccion: ";
      cin >> tipoInfraccion;

      cout << "Ingrese el motivo de la infraccion: ";
      cin >> motivo;

      cout << "Ingrese la gravedad de la infraccion: ";
      cin >> gravedadInfraccion;

      switch (gravedadInfraccion)
      {
      case 'L':
        vTotalL += valorMultaL;
        break;
      
      case 'M':
        vTotalM += valorMultaM;
        break;
      
      case 'G':
        vTotalG += valorMultaG;
        if (tipoInfraccion == 3 || tipoInfraccion == 4) {
          totalInfG34 += 1;
        }
        break;

      default:
        printf("Ingreso una gravedad no valida\n");
        break;
      }
    }

    printf("El total a pagar para infracciones L es: %d\n", vTotalL);
    printf("El total a pagar para infracciones M es: %d\n", vTotalM);
    printf("El total a pagar para infracciones G es: %d\n", vTotalG);

    if (totalInfG34 > 3) {
      printf("Clausurar fabrica\n");
    }
    return 0;
}