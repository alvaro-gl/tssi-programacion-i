#include <iostream>
using namespace std;

/* ​ Ingresar e informar valores, mientras que el valor ingresado no sea negativo. 
    Informar la cantidad de valores ingresados. */

int main() {

    int valorIngresado = 0, contador = 0;

    while (valorIngresado >= 0) {
        cout << "Ingrese un valor: ";
        cin >> valorIngresado;

        if (valorIngresado >= 0) {
        contador += 1;
        }
    }

    printf("La cantidad de valores no negativos ingresados fue: %d\n", contador);

    return 0;
}