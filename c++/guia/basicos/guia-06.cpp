#include <iostream>
using namespace std;

/* Dadas dos fechas informar cual es la más reciente. Determine cuáles serían los datos de entrada y las leyendas
a informar de acuerdo al proceso solicitado. */

int getAnio(int fecha) {
    return fecha / 10000;
}

int getMes(int fecha) {
    return (fecha - getAnio(fecha) * 10000) / 100;
}

int getDia(int fecha) {
    return (fecha - getAnio(fecha) * 10000 - getMes(fecha) * 100);
}

int main() {
    int fechaUno, fechaDos, anioUno, anioDos, mesUno, mesDos, diaUno, diaDos;

    cout << "Ingrese la primer fecha (AAAAMMDD): ";
    cin >> fechaUno;

    cout << "Ingrese la segunda fecha (AAAAMMDD): ";
    cin >> fechaDos;

    anioUno = getAnio(fechaUno);
    mesUno = getMes(fechaUno);
    diaUno = getDia(fechaUno);

    anioDos = getAnio(fechaDos);
    mesDos = getMes(fechaDos);
    diaDos = getDia(fechaDos);

    if (anioUno < anioDos) {
        printf("La fecha mas reciente es %02d/%02d/%4d.\n", diaUno, mesUno, anioUno);
    } else if (anioUno > anioDos) {
        printf("La fecha mas reciente es %02d/%02d/%4d.\n", diaDos, mesDos, anioDos);
    } else {
        if (mesUno < mesDos) {
            printf("La fecha mas reciente es %02d/%02d/%4d.\n", diaUno, mesUno, anioUno);
        } else if (mesUno > mesDos) {
            printf("La fecha mas reciente es %02d/%02d/%4d.\n", diaDos, mesDos, anioDos);
        } else {
            if (diaUno < diaDos) {
                printf("La fecha mas reciente es %02d/%02d/%4d.\n", diaUno, mesUno, anioUno);
            } else if (diaUno > diaDos) {
                printf("La fecha mas reciente es %02d/%02d/%4d.\n", diaDos, mesDos, anioDos);
            } else {
                printf("Las fechas son iguales.\n");
            }
        }
    }

    return 0;
}