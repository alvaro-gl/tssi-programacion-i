#include <iostream>
using namespace std;

/* Dada una terna de números naturales que representan al día, al mes y al año de una determinada
   fecha informarla como un solo número natural de 8 dígitos con la forma (AAAAMMDD). */

int main() {
    int dia, mes, anio;

    cout << "Ingrese el dia (DD): ";
    cin >> dia;

    cout << "Ingrese el mes (MM): ";
    cin >> mes;

    cout << "Ingrese el anio (AAAA): ";
    cin >> anio;

    // Como seria desde pseudocodigo, _Mostrar_:
    // cout << (anio * 10000) + (mes * 100) + dia << "\n";

    // Modo C++:
    printf("%4d%02d%02d\n", anio, mes, dia);

    return 0;
}