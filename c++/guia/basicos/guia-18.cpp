#include <iostream>
using namespace std;

/* Dado un valor M determinar y emitir un listado con los M primeros múltiplos de 3 que no lo sean de 5,
   dentro del conjunto de los números naturales. */

int main() {

    int cantidadMultiplos = 0;

    cout << "Ingrese la cantidad de multiplos deseados: ";
    cin >> cantidadMultiplos;

    if (cantidadMultiplos <= 0) {
        cout << "La cantidad de multiplos no puede ser no positiva";
        return 1;
    }

    int multiplos[cantidadMultiplos];

    int cursor = cantidadMultiplos;
    int arrayIndex = 0;

    for (int i = 0; i < cursor; i++) {
        int multiplo = i * 3;

        if (multiplo == 0) {
            multiplos[arrayIndex] = multiplo;
            arrayIndex += 1;
            continue;
        } else if (multiplo != 0 && multiplo % 5 != 0){
            multiplos[arrayIndex] = multiplo;
            arrayIndex += 1;
        } else {
            // Jugando con fuego
            cursor += 1;
        }
    }

    printf("Los primeros %d multiplos de 3 que no son multiplos de 5 son: ", cantidadMultiplos);

    for (int i = 0; i < cantidadMultiplos; i++) {
        printf(" %d ", multiplos[i]);
    }

    printf("\n");

    return 0;
}