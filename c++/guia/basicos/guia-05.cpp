#include <iostream>
using namespace std;

// Dados dos valores enteros y distintos, emitir una leyenda apropiada que informe cuál es el mayor entre ellos.

int main() {
    int a, b;

    cout << "Ingrese el primer numero: ";
    cin >> a;

    cout << "Ingrese el segundo numero: ";
    cin >> b;

    if (a > b) {
        printf("El mayor es %d.\n", a);
    } else {
        printf("El mayor es %d.\n", b);
    }

    return 0;
}