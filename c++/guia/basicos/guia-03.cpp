#include <iostream>
using namespace std;

/* Dada un número entero de la forma (AAAAMMDD), que representa una fecha valida mostrar el dia, mes y
   año que representa. */

int main() {

    int fecha, dia, mes, anio;

    cout << "Ingrese la fecha (AAAAMMDD): ";
    cin >> fecha;

    anio = fecha / 10000;
    mes = (fecha - (anio * 10000)) / 100;
    dia = (fecha - anio * 10000) - (mes * 100);

    printf("%02d/%02d/%4d\n", dia, mes, anio);

    return 0;
}
