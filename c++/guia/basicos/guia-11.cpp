#include <iostream>
using namespace std;

/* e ingresa una edad, mostrar por pantalla alguna de las siguientes leyendas:
-​ ‘menor’ si la edad es menor o igual a 12
-​ ‘cadete’ si la edad está comprendida entre 13 y 18
-​ ‘juvenil’ si la edad es mayor que 18 y no supera los 26
-​ ‘mayor’ en el caso que no cumpla ninguna de las condiciones anteriores */


int main() {
    int edad;
    string categoria;

    cout << "Ingrese la edad: ";
    cin >> edad;

    if (edad > 25) {
        categoria = "mayor";
    } else if (edad > 18) {
        categoria = "juvenil";
    } else if (edad > 12) {
        categoria = "cadete";
    } else {
        categoria = "menor";
    }

    /* Uso de std::string::c_str() para obtener un C like string
    https://stackoverflow.com/questions/10865957/printf-with-stdstring */

    printf("La categoria es %s.\n", categoria.c_str());

    return 0;
}