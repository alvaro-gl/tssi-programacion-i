#include <iostream>
using namespace std;

/* Se dispone de un lote de valores enteros positivos que finaliza con un número negativo.
   El lote está dividido en sublotes por medio de valores cero. Desarrollar un programa que determine e informe:
     a) por cada sublote el promedio de valores
     b) el total de sublotes procesados
     c) el valor máximo del conjunto, indicando en que sublote se encontró y la posición relativa del mismo dentro del sublote
     d) valor mínimo de cada sublote */

int main() {
    int contador = 0, sumador = 0, totalSL = 0, valorMax = 0, posMax = 0, slMax = 0, valorMin = 0;
    int trigger = 0;
    int firstRun = true;

    while (trigger >= 0) {
        cout << "Ingrese un numero: ";
        cin >> trigger;

        if (trigger <= 0) {
            // Si es la primera vez que se corre el programa y el primer valor es cero o negativo, salimos
            if (firstRun) {
                printf("\n--- No se ingresaron valores para computar ----\n\n");
                trigger = -1;
                totalSL = 0;
                break;
            } else {
                // Si se ingresa un cero y luego otro cero, salimos
                if (sumador == 0) {
                    printf("\n--- No se ingresaron valores para computar ---\n\n");
                    trigger = -1;
                    break;
                }

                printf("\nEl promedio de valores de este sublote fue: %d\n", (sumador / contador));
                printf("El valor minimo del sublote fue: %d\n\n", valorMin);
                totalSL += 1;
                sumador = contador = 0;

                // Si se ingreso un numero negativo salimos
                if (trigger < 0) { break; }
            }
        } else {

            if (contador == 0) {
                valorMin = trigger;
            }

            sumador += trigger;
            contador += 1;

            if (trigger > valorMax) {
                valorMax = trigger;
                posMax = contador;
                slMax = totalSL;
            }

            if (trigger < valorMin) {
                valorMin = trigger;
            }
        }
        firstRun = false;
    }

    printf("Resumen\n");
    printf("-------\n");
    printf("El total de sublotes fue: %d\n", totalSL);
    printf("El maximo valor fue: %d\n", valorMax);
    printf("La posicion relativa del maximo fue: %d\n", posMax);
    printf("El sublote con el maximo fue: %d\n", (slMax + 1));

    return 0;
}