#include <iostream>
using namespace std;

/* A partir de un valor entero ingresado por teclado, se pide informar:
   a) La quinta parte de dicho valor
   b) El resto de la división por 5
   c) La séptima parte del resultado del punto a) */

int main() {
    
    float numero;

    cout << "Ingrese un numero: ";
    cin >> numero;

    printf("La quinta parte es: %4.2f\n", numero / 5);
    printf("El resto de la division por cinco es: %1d\n", int(numero) % 5);
    printf("La septima parte de la quinta parte es: %4.2f\n", numero / 5 / 5);

    return 0;
}