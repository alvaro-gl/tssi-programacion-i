#include <iostream>
using namespace std;

/* Informar los primeros 100 números naturales y su sumatoria. */


int main() {

    int counter = 0;

    printf("Los primeros 100 numeros naturales son: ");
    
    for (int i = 1; i <= 100; i++) {
        counter += i;
        printf("%d ", i);
    }

    printf("\nLa suma de los primeros 100 numeros naturales es %d.\n", counter);

    return 0;
}