#include <iostream>
using namespace std;

/* Dados 10 valores informar el mayor. */


int main() {

    int max = 0;

    srand(time(0));

    for (int i = 0; i <= 10; i++) {
        int valor = (rand() % 100);

        if (valor > max) {
            max = valor;
        }
    }

    printf("El valor maximo fue %d \n", max);

    return 0;
}