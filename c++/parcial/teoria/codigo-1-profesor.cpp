#include <iostream>

using namespace std;
/*

Completar las funciones faltantes para que el siguiente programa realice lo siguiente:

Dados dos vectores de numeros enteros ordenados ascendentemente obtener e imprimir por consola los valores de un tercer vector con los elementos que solo eestán en uno de los dos conjuntos. El vector resultante tambien debe estar ordenado en forma ascendente.

ej:

A={-12, -8, -2, 0, 1, 5, 8, 10}    y B={-7, -2, 1, 5, 9, 11}   entonces c={-12,-8,-7,0,8,9,10,11}


*/

// Esta funcion debe recibir dos vectores y completar un tercero con los elementos que están en solo uno de los dos conjuntos.
void distintos(int vecA[], int cantA, int vecB[], int cantB, int vecC[], int &k)
{
    int i = 0, j = 0;
    k = 0;

    while (i < cantA && j < cantB)
    {
        if (vecA[i] == vecB[j])
        {
            i++;
            j++;
        }
        else if (vecA[i] < vecB[j])
        {
            vecC[k] = vecA[i];
            i++;
            k++;
        }
        else
        {
            vecC[k] = vecB[j];
            j++;
            k++;
        }
    }

    while (i < cantA)
    {
        vecC[k] = vecA[i];
        i++;
        k++;
    }

    while (j < cantB)
    {
        vecC[k] = vecB[j];
        j++;
        k++;
    }
}

void imprimir(int vec[], int cant)
{
    cout << "Copiar los valores entre numerales #99999999#" << endl
         << "=========================================="
         << endl
         << "#";
    for (int i = 0; i < cant; i++)
    {
        cout << vec[i] << "";
    }
    cout <<"#"
         << endl
         << "==========================================" << endl;
}

int main()
{
    int vecA[] = {-3000,
                  -2977,
                  -2975,
                  -2895,
                  -2831,
                  -2798,
                  -2721,
                  -2716,
                  -2560,
                  -2554,
                  -2461,
                  -2390,
                  -2231,
                  -2162,
                  -2130,
                  -2065,
                  -2024,
                  -1994,
                  -1871,
                  -1844,
                  -1751,
                  -1681,
                  -1586,
                  -1577,
                  -1559,
                  -1525,
                  -1307,
                  -1138,
                  -1007,
                  -977,
                  -932,
                  -924,
                  -856,
                  -766,
                  -601,
                  -576,
                  -544,
                  -530,
                  -518,
                  -506,
                  -489,
                  -484,
                  -458,
                  -125,
                  -50,
                  24,
                  25,
                  39,
                  43,
                  98,
                  116,
                  120,
                  291,
                  436,
                  472,
                  483,
                  486,
                  488,
                  501,
                  578,
                  698,
                  711,
                  858,
                  1038,
                  1054,
                  1153,
                  1160,
                  1166,
                  1216,
                  1291,
                  1404,
                  1432,
                  1447,
                  1504,
                  1553,
                  1584,
                  1819,
                  1844,
                  1862,
                  1886,
                  1953,
                  2008,
                  2094,
                  2107,
                  2154,
                  2214,
                  2232,
                  2263,
                  2316,
                  2350,
                  2391,
                  2401,
                  2440,
                  2486,
                  2501,
                  2609,
                  2626,
                  2655,
                  2793,
                  2874};
    int vecB[] = {-2985,
                    -2960,
                    -2894,
                    -2886,
                    -2833,
                    -2823,
                    -2812,
                    -2803,
                    -2750,
                    -2727,
                    -2686,
                    -2680,
                    -2668,
                    -2576,
                    -2567,
                    -2543,
                    -2435,
                    -2373,
                    -2086,
                    -2064,
                    -2041,
                    -1955,
                    -1951,
                    -1879,
                    -1869,
                    -1779,
                    -1692,
                    -1646,
                    -1559,
                    -1516,
                    -1507,
                    -1432,
                    -1260,
                    -1239,
                    -1210,
                    -1115,
                    -1034,
                    -1034,
                    -1028,
                    -1011,
                    -933,
                    -827,
                    -713,
                    -703,
                    -652,
                    -618,
                    -503,
                    -318,
                    -143,
                    -132,
                    -76,
                    -17,
                    -9,
                    166,
                    220,
                    335,
                    601,
                    677,
                    719,
                    760,
                    763,
                    778,
                    873,
                    877,
                    918,
                    930,
                    1075,
                    1108,
                    1161,
                    1323,
                    1330,
                    1363,
                    1467,
                    1508,
                    1542,
                    1638,
                    1649,
                    1771,
                    1927,
                    1954,
                    1976,
                    1995,
                    2008,
                    2100,
                    2210,
                    2211,
                    2452,
                    2470,
                    2471,
                    2471,
                    2503,
                    2578,
                    2608,
                    2695,
                    2700,
                    2794,
                    2847,
                    2864,
                    2869,
                    2890};
    int vecC[176] = {0};

    int cant = 0;
    distintos(vecA, 100, vecB, 76, vecC, cant);

    imprimir(vecC, cant);
    system("pause");
}