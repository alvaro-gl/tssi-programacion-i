#include <iostream>
#include <string>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

TEST_CASE ( "Algoritmo mayor" ) {
    int r = mayor(5, 7, 9);

    REQUIRE( r == 9 );
}

TEST_CASE ( "Algoritmo menor" ) {
    int r = menor(5, 7, 9);

    REQUIRE( r == 5 );
}

TEST_CASE ( "Algoritmo medio" ) {
    int men = mayor(5, 7, 9);
    int may = menor(5, 7, 9);

    int med = medio(5, 7, 9, men, may);

    REQUIRE( med == 7 );
}

TEST_CASE ( "Dibujar cuadrado" ) {
    string esperado = "****\n****\n****\n****\n";
    string r = dibujarCuadrado(4);

    CHECK_THAT( esperado, Catch::Equals(r) );
}

TEST_CASE ( "Dibujar triangulo" ) {
    string esperado = "*\n**\n***\n****\n";
    string r = dibujarTriangulo(4);

    CHECK_THAT( esperado, Catch::Equals(r) );
}

TEST_CASE ( "Crapulo - Calculo simple" ) {
    int r = crapulo(13);

    REQUIRE( r == 4 );
}

TEST_CASE ( "Enviar un SMS" ) {
    int bateriaPre = 100;
    int bateriaPost = bateriaPre;

    enviarSMS(bateriaPost);

    REQUIRE(bateriaPost == --bateriaPre);
}

TEST_CASE ( "Realizar una llamada con suficiente bateria" ) {
    int bateriaPre = 100;
    int bateriaPost = bateriaPre;
    int minutos = 100;

    realizarLlamada(bateriaPost, minutos);

    REQUIRE(bateriaPost == bateriaPre - 8);
    REQUIRE(minutos == 100);
}

TEST_CASE ( "Realizar una llamada sin suficiente bateria" ) {
    int bateriaPre = 6;
    int bateriaPost = bateriaPre;
    int minutos = 100;

    realizarLlamada(bateriaPost, minutos);

    REQUIRE(bateriaPost == bateriaPre - 6);
    REQUIRE(minutos == 75);
}

TEST_CASE ( "Tomar una foto con suficiente bateria" ) {
    int bateriaPre = 100;
    int bateriaPost = bateriaPre;
    int fotos = 5;

    tomarFotos(bateriaPost, fotos);

    REQUIRE(bateriaPost == bateriaPre - (fotos * 6));
}

TEST_CASE ( "Tomar fotos sin suficiente bateria" ) {
    int bateriaPre = 10;
    int bateriaPost = bateriaPre;
    int fotos = 5;

    tomarFotos(bateriaPost, fotos);

    REQUIRE(bateriaPost == bateriaPre - 6);
}