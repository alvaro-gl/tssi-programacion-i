#include <iostream>
#include <limits>
#include "funciones.h"
#define MAX_HAB_POR_TIPO 20
#define MAX_HABITACIONES 60
#define MAX_HOTELES 30
#define TOP 3

using namespace std;

// UI Basicos
void uiMotd () {
    printf("##### Bienvenido a Despegar Dial Up #####\n");
    printf(">> Presione Ctrl-C para finalizar el proceso.\n");
}

void uiMenu (int &s) {
    s = -1;
    while (s < 0 || s > 3) {
        printf("\nSeleccione la accion a realizar: ");
        printf("\n--------------------------------\n");
        printf("-- 1: Realizar reserva.\n");
        printf("-- 2: Liberar habitacion.\n");
        printf("-- 3: Listar destinos con mayor ocupacion segun temporada.\n");
        cout << "Opcion: ";
        cin >> s;

        if (cin.fail() || s < 0 || s > 3) {
            // Que dificil es sanitizar input en este lenguaje
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n");
        }
    }
}

// UI ingreso de datos
void uiPersonas (int &personas) {
    personas = -1;
    while (personas < 0 || personas > 8) {
        printf("\nCantidad de personas");
        printf("\n--------------------\n");
        printf(">> Ingrese la cantidad de personas para la reserva: ");
        scanf("%d", &personas);

        if (cin.fail() || personas < 0 || personas > 8) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("El numero de personas es superior al limite o el valor ingresado no es valido!\n");
        }
    }
}

void uiHabitacion (int &nroHabitacion) {
    nroHabitacion = -1;

    while (nroHabitacion < 0 || nroHabitacion > 60) {
        printf("\nNumero de habitacion reservada");
        printf("\n------------------------------\n");
        printf(">> Ingrese el numero de habitacion: ");
        scanf("%d", &nroHabitacion);

        if (cin.fail() || nroHabitacion < 0 || nroHabitacion > 60) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("El numero de habitacion es superior al limite o el valor ingresado no es valido!\n");
        }
    }
}

// UI comun
int uiDestino (Destino destinos[]) {
    string destino;
    // Tengo destinos con espacios asi que tenia que usar getline.
    // Por lo que entendi tenia que consumir el new line del menu principal con cin.get()
    cin.get();
    printf("\nBusqueda de destino");
    printf("\n-------------------\n");
    printf(">> Ingrese el destino del cliente: ");
    getline(cin, destino);
        
    int d = buscarDestino(destino, destinos);
    if (d != -1) {
        printf(">> Destino encontrado!\n");
        printf(">> El destino es %s (Temporada %c)\n", destinos[d].nombre.c_str(), destinos[d].temporada);
    } else {
        printf(">> Destino no encontrado!\n");
    }
    return d;
}

// UI Reserva
int uiHabitacionLibre (Habitacion habitaciones[], int personas) {
    int h = buscarHabitacionDisponible(habitaciones, personas);
    if (h != -1) {
        printf(">> Habitacion encontrada!\n");
    } else {
        printf(">> No hay habitaciones disponibles!\n");
    }

    return h;
}

int uiReservarHabitacion (Habitacion &h) {
    printf("\nReserva");
    printf("\n-------\n");
    
    char c = 'X';
    int r = -1;

    while (c != 's' && c != 'n'){
        printf(">> Reservar la habitacion? (s/n): ");
        cin >> c;

        if (cin.fail() || c != 's' && c != 'n') {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n"); 
        } else {
            if (h.estado != 'O') {
                if (c == 's') {
                    h.estado = 'O';
                    printf("Reserva realizada!\n");
                    r = 0;
                } else if (c == 'n') {
                    printf("Reserva cancelada!\n");
                } else {
                    printf("Respuesta no valida!\n");
                }
            } else {
                printf("Error! La habitacion solicitada se encuentra ocupada!\n");
            }
        }
    }
    return r;
}

void uiFinalReserva (string hotel, string ciudad, int habitacion, int capacidad, int personas) {
    printf("\nDatos de la reserva ");
    printf("\n-------------------\n");
    printf(">> Reserva realizada en el hotel %s en la ciudad de %s\n", hotel.c_str(), ciudad.c_str());
    printf(">> Reservada la habitacion Numero %d (Capacidad %d personas)\n", habitacion + 1, capacidad);
    printf(">> Cantidad de personas: %d\n", personas);
}

// UI Liberar habitacion
int uiHabitacionOcupada (Habitacion habitaciones[], int h) {
    int r = -1;
    printf("\nBusqueda de reserva");
    printf("\n-------------------\n");

    if (habitaciones[h].estado == 'O') {
        // Asumimos que esta es la reserva
        printf(">> Reserva encontrada!\n");
        r = 0;
    } else {
        printf("Error! La habitacion indicada se encuentra libre!\n");
    }
    return r;
}

int uiLiberarHabitacion (Habitacion &h) {
    printf("\nLiberar habitacion");
    printf("\n------------------\n");

    char c = 'X';
    int r = -1;

    while (c != 's' && c != 'n'){
        printf(">> Liberar la habitacion? (s/n): ");
        cin >> c;

        if (cin.fail() || c != 's' && c != 'n') {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n"); 
        } else {
            if (c == 's') {
                h.estado = 'D';
                printf("Habitacion liberada!\n");
                r = 0;
            } else if (c == 'n') {
                printf("Accion cancelada!\n");
            } else {
                printf("Respuesta no valida!\n");
            }
        }
    }
    return r;
}

void uiFinalLiberacion (string hotel, string ciudad, int habitacion) {
    printf("\nDatos de la liberacion");
    printf("\n----------------------\n");
    printf(">> Reserva liberada en el hotel %s en la ciudad de %s\n", hotel.c_str(), ciudad.c_str());
    printf(">> Liberada la habitacion Numero %d\n", habitacion + 1);
}

void uiTopDestinos(Destino destinos[]) {
    printf("\nTop %d de destinos por Temporada", TOP);
    printf("\n-------------------------------\n");

    string temporada;
    char t;
    Destino d[TOP];

    // Restrinjo los valores segun el enunciado
    while (temporada != "Invierno" && temporada != "Verano"){
        printf(">> Ingrese la temporada: ");
        cin >> temporada;

        if (cin.fail() || temporada != "Invierno" && temporada != "Verano") {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n"); 
        } else {
            if (temporada == "Invierno") {
                t = 'I';
            } else {
                t = 'V';
            }
            topDestinosTemporada(destinos, t, d);

            int ranking = TOP;
            for (int i = 0; i < TOP; i++) {
                int oc = ocupacion(d[i].hotel);
                printf("El destino numero %d", ranking); 
                if (oc == 1) {
                    printf(" es %s con %d habitacion ocupada\n", d[i].nombre.c_str(), oc);
                } else {
                    printf(" es %s con %d habitaciones ocupadas\n", d[i].nombre.c_str(), oc);
                }
                ranking--;
            }
        }
    }
    
}

int main () {
    // Setup inicial
    Destino destinos[MAX_HOTELES];
    cargarDestinos(destinos);
    ordenarDestinos(destinos);

    uiMotd();

    while (true) {
        int s;
        uiMenu(s);

        switch (s) {
            case 1: {
                // Busqueda del destino
                int d = uiDestino(destinos);
                if (d != -1) {
                    // Busqueda de habitacion
                    int personas;
                    uiPersonas(personas);
                    int h;
                    if (d != -1) {
                        h = uiHabitacionLibre(destinos[d].hotel.habitaciones, personas);
                    }        
                    // Reserva de habitacion
                    int r;
                    if (h != -1) {
                        r = uiReservarHabitacion(destinos[d].hotel.habitaciones[h]);
                    }
                    // Mensaje final
                    if (r != -1) {
                        uiFinalReserva(destinos[d].hotel.nombre, destinos[d].nombre, h, destinos[d].hotel.habitaciones[h].okupas, personas);
                    }
                }
                break;
            }
            case 2: {
                // Busqueda del destino
                int d = uiDestino(destinos);
                if (d != -1) {
                    // Busqueda de habitacion
                    int h;
                    uiHabitacion(h);
                    int hr = h - 1;
                    int r = uiHabitacionOcupada(destinos[d].hotel.habitaciones, hr);
                    // Operation freedom
                    int l;
                    if (r != -1) {
                        l = uiLiberarHabitacion(destinos[d].hotel.habitaciones[hr]);
                    }
                    // Mensaje Final
                    if (l != -1) {
                        uiFinalLiberacion(destinos[d].hotel.nombre, destinos[d].nombre, hr);
                    }
                }
                break;
            }
            case 3: {
                uiTopDestinos(destinos);
                break;
            }
        }
    }
}