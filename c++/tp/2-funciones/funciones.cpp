int cuentaRegresivaPura(int valor) {
    if (valor == 0) {
        return 0;
    }

    return valor + cuentaRegresivaPura(valor - 1);
}