#include <iostream>
#include "funciones.h"
using namespace std;

/*
Desarrolle una función recursiva que dado un número entero positivo permita obtener
la sumatoria de todos los números enteros que se encuentran entre 0 y el número ingresado.
Por ejemplo:
  - a. Ingresado el número 5, la función debe devolver 15 porque 0 + 1 + 2 + 3 + 4 + 5 = 15
  - b. OPCIONAL modifique la función anterior para que se impriman los resultados parciales. Por ejemplo, para el número 5, la salida
       sería:
                0
                1
                3
                6
                10
                15
*/

/* ATENCION: compilar y correr este codigo en macOS Catalina puede devolver valores erroneos */

int cuentaRegresivaText(int valor) {
    if (valor == 0) {
        printf("%d\n", 0);
        return 0;
    }

    int retorno = valor + cuentaRegresivaText(valor - 1);

    printf("%d\n", retorno);

    return retorno;
}

int main() {

    printf(">> Probando la cuenta regresiva recursiva pura: \n");
    printf("Ejecutando para el numero 5: %d\n", cuentaRegresivaPura(5));
    printf("Ejecutando la cuenta regresiva con texto (ejercicio opcional) tambien para 5: \n");
    cuentaRegresivaText(5);
    return 0;
}
